<html>
  <head>
      
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
       google.charts.load('current', {'packages':['bar'],'language': 'es'});
       google.charts.setOnLoadCallback(drawChart);
      
        

     function drawChart() {
        var data = google.visualization.arrayToDataTable([
        //  ['Mes', 'Sales', 'Expenses', 'Profit'],
        //  ['2014', 1000, 400, 200],
        //  ['2015', 1170, 460, 250],
        //  ['2016', 660, 1120, 300],
          //['2017', 1030, 540, 350],
        //  ['Totales', 4000, 3540, 350]
        //]);
      

       <?php
        header("Content-Type: text/html;charset=utf-8");
        setlocale(LC_TIME, 'spanish');
        

        $conexion=mysqli_connect("localhost","root","","asistencias") or die ("Problema con la conexion");

        mysqli_select_db($conexion,"asistencias")or die("Cannot Select  Bass"); 

        $q = $_POST['q'];
        $mes = $_POST['mes'];
        $mesnum= date('m',strtotime($mes));
        $mes=strftime("%B",strtotime($mes));
        $sql = "select DISTINCT ID_Subgrupo from  CargaMaestro where  CargaMaestro.ID_Maestro= 1022";
        
        //echo "data.addColumn('number', '".$mes."');";
        if(date('m')>=7){
            $fechain=8;
            $fechafin=(date('m'));
         }
         else{
            $fechain=1;
            $fechafin=(date('m'));
         }
        
        $materias = mysqli_query($conexion,$sql) or die("lul");
        if(mysqli_num_rows($materias)) {
         
         $matarray= array();
         $numarray= array();
         $totales= array();
         $x=0;
         while($row = mysqli_fetch_assoc($materias)) {
             foreach($row as $key=>$value) {
                // echo $value;
                 $matarray[$x]=$value;
                 $ii=0;
                 $total=0;

                 for ($i = $fechain; $i <= $fechafin; $i++) {
                     
                     
                     $sql2 = "SELECT count(`Asistencia`) FROM `lista` WHERE `ID_Subgrupo`= '".$value."' and MONTH(`fecha`) = ".$i."  and `Asistencia` = 0";
                     $sql2res = mysqli_query($conexion,$sql2) or die("lul");
                     while($row2 = mysqli_fetch_assoc($sql2res)) {
                         foreach($row2 as $key2=>$value2) {
                              $numarray[$ii][0]=$i;
                              $numarray[$ii][$x+1]=$value2;
                              $ii++;
                              $total+=$value2;
                             }
                         }
                                               
                     }
                     $totales[$x]=$total;
                         $x++;
                 }
             }
          }
          $matarrcount=count($matarray);
          $tablerow=count($numarray);
          $tablecol=count($numarray[0]);
          $totcount=count($totales);
          echo "['Mes',";
          for ($i=0; $i < $matarrcount; $i++){
              echo "'".$matarray[$i]."'";
              if($i!=$matarrcount-1){
                  echo ",";
              }
          }
          echo "],";

          for ($i = 0; $i < $tablerow; $i++) {
            echo '[';
            for ($j = 0; $j <$tablecol; $j++) {
                if($j==0){
                    $var1=ucfirst(strftime("%B",mktime(0, 0, 0,$numarray[$i][$j])));
                    echo "'".$var1."'";
                }
                else{
                    echo $numarray[$i][$j];
                }
              if ($j!=$tablecol-1)
              {
                echo ',';
              }
            }
            echo ']';
            if ($i!=$tablerow-1)
            {
              echo ',';
            }
            
          }
          echo ",['Totales',";
          for ($i=0; $i < $totcount; $i++){
            echo $totales[$i];
            if($i!=$totcount-1){
                echo ",";
            }
          }
          echo "]";

          echo ']);';
          

      

                 

       ?>
    

       //data.addColumn('number', 'Febrero');
       //data.addColumn('number', 'lel ');
      // data.addColumn('number', 'The Avengers');
       //data.addColumn('number', 'Transformers: Age of Extinction');
 
       /*data.addRows([
         [3,  37.8, 80.8, 41.8],
         [2,  30.9, 69.5, 32.4],
         [3,  25.4,   57, 25.7],
         [4,  11.7, 18.8, 10.5],
         [5,  11.9, 17.6, 10.4],
         [6,   8.8, 13.6,  7.7],
         [7,   7.6, 12.3,  9.6],
         [8,  12.3, 29.2, 10.6],
         [9,  16.9, 42.9, 14.8],
         [10, 12.8, 30.9, 11.6],
         [11,  5.3,  7.9,  4.7],
         [12,  6.6,  8.4,  5.2],
         [13,  4.8,  6.3,  3.6],
         [14,  4.2,  6.2,  3.4]
       ]); */
 
       var options = {
         chart: {
           title: 'Faltas totales por materia',
           subtitle: ''
         },
         titleTextStyle: {
        color: 'black'
    },
    subtitleTextStyle: {
        color: 'black'
    },
    hAxis: {
        textStyle: {
            color: 'black'
        },
        titleTextStyle: {
            color: 'black',fontSize: 16
        }
    },
    vAxis: {
        textStyle: {
            color: 'black'
        },
        titleTextStyle: {
            color: 'black'
        }
    },
    legend: {
        textStyle: {
            color: 'black'
        }
    },
    
         width: 900,
         height: 500
       };
 
       var chart = new google.charts.Bar(document.getElementById('alumnos'));
 
       chart.draw(data, google.charts.Bar.convertOptions(options));
     }
    </script>
  </head>
  <body>
    <div id="chart" style="width: 800px; height: 500px;"></div>
    
    
  </body>
</html>

