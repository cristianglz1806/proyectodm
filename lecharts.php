<html>
<head>
<title>LeCharts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="tabs.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- <script>
function showUser(str) {
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","tabla3.php?q="+str,true);
        xmlhttp.send();
    }
}
function carga()
{
	var ajax='php/carga10.php';
	//alert(pagina+" "+categoria+" "+ptotal);
	data={'pagina':pagina, 'categoria':categoria, 'final':ptotal};
	$.post(ajax,data,function(response){
		$('#principal').html(response);
	});
	//alert(ptotal);
}
</script> -->
</head>
<body>


<div id="lel"  type="text">Le graficas</div>
<br><br><br>

<?php
header("Content-Type: text/html;charset=utf-8");
setlocale(LC_TIME, 'spanish');
$conexion=mysqli_connect("localhost","root","","asistencias") or die ("Problema con la conexion");

mysqli_select_db($conexion,"asistencias")or die("Cannot Select  Bass");


//$sql = "select DISTINCT subgrupo.ID_subgrupo from Materia, Subgrupo, CargaMaestro where subgrupo.ID_Materia=Materia.ID_Materia and Subgrupo.ID_Maestro=CargaMaestro.ID_Maestro and CargaMaestro.ID_Maestro= 1022";


$sql = "select DISTINCT ID_Subgrupo from  CargaMaestro where  CargaMaestro.ID_Maestro= 1022";
echo '<div id="chart">Tipo de grafica:';
?>
<select name="type" id="type" onchange="dis(this.value); ">
    <option value="1">Faltas por materia/mes</option>
    <option value="3">Faltas totales por materia</option>
    <option value="2">Faltas por alumno</option>
 </select>
<?php
echo ' Materia: ';
//echo '<p> </p>';

$materias = mysqli_query($conexion,$sql) or die("lul");
if(mysqli_num_rows($materias)) {
  echo '<select id="idmateria" name="materia" disabled onchange="cargaal();">';
    while($materia = mysqli_fetch_row($materias)) {
        
        foreach($materia as $key=>$value) {
          echo '<option value="'.$value.'">'.$value.'</option>';
          
        }
      }
    echo '</select>';
  }

  echo ' <button onclick="chart1();"> Generar grafica </button> </div>';
 
?>
<script>

function dis(val)
{
    if(val==2 || val==3){
        document.getElementById("mes").disabled=true;
    }
    if(val==1){
        document.getElementById("mes").disabled=false;
    }

    if (val==1 || val== 3){
        document.getElementById("idmateria").disabled=true;
        document.getElementById("alumnoselect").disabled=true;
    
        var ajax='alumnos.php';
    
    }
    else{
        if (document.getElementById("type").value=="2"){
        var ajax='alumnos.php';
    var idmateria= document.getElementById("idmateria").value;
    
	//alert(pagina+" "+categoria+" "+ptotal);
	data={'q':idmateria};
    
	$.post(ajax,data,function(response){
		$('#txtHint').html(response);
	});
	//alert(ptotal);
    }
        document.getElementById("idmateria").disabled=false;
        document.getElementById("alumnoselect").disabled=false;
    
    }
    
	
	
}

function chart1()
{
    if (document.getElementById("type").value=="1")
    {
        var ajax='chart1.php';
    var idmateria= document.getElementById("idmateria").value;
    var mes=document.getElementById("mes").value;
	//alert(pagina+" "+categoria+" "+ptotal);
	data={'q':idmateria, 'mes':mes};
    
	$.post(ajax,data,function(response){
		$('#alumnos').html(response);
	});
    }
    if (document.getElementById("type").value=="3")
    {
        var ajax='chart2.php';
    var idmateria= document.getElementById("idmateria").value;
    var mes=document.getElementById("mes").value;
	//alert(pagina+" "+categoria+" "+ptotal);
	data={'q':idmateria, 'mes':mes};
    
	$.post(ajax,data,function(response){
		$('#alumnos').html(response);
	});
    }
    if (document.getElementById("type").value=="2")
    {
        var ajax='chart3.php';
    var idmateria= document.getElementById("idmateria").value;
    var mes=document.getElementById("mes").value;
	//alert(pagina+" "+categoria+" "+ptotal);
	data={'q':idmateria, 'mes':mes};
    
	$.post(ajax,data,function(response){
		$('#alumnos').html(response);
	});
    }
    
    
    
	//alert(ptotal);
    
	
}

function cargaal()
{
    if (document.getElementById("type").value=="2"){
        var ajax='alumnos.php';
    var idmateria= document.getElementById("idmateria").value;
    
	//alert(pagina+" "+categoria+" "+ptotal);
	data={'q':idmateria};
    
	$.post(ajax,data,function(response){
		$('#txtHint').html(response);
	});
	//alert(ptotal);
    }
	
}

function showUser(str) {
    var q=document.getElementById("datepicker").value;
    document.getElementById("datepicker").disabled=false;
    document.getElementById("grupo").value=str;
    
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","tabla3.php?r="+str + "&q=" +q,true);
        xmlhttp.send();
    }
}



$(function() {
    $('#datepicker').datepicker( {
    		minDate: "-120",
        maxDate: "+12",
        changeMonth: true,  
        dateFormat: 'MM',
        onClose: function(dateText, inst) { 
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            
            
        }
    }).datepicker("setDate", new Date());;
});
</script>
<p>Mes: 
<?php
    if(date('m')>=7){
        $fechain=8;
        $fechafin=(date('m'));
     }
     else{
        $fechain=1;
        $fechafin=(date('m'));
     }

     echo '<select name="type" id="mes" >';
     for ($i = $fechain; $i <= $fechafin; $i++){
        $var1=ucfirst(strftime("%B",mktime(0, 0, 0,$i)));
        echo '<option value="1999-'.$i.'-01">'.$var1.'</option>';
     }
     echo '</select>';
?>


 Grupo: <input type="text" id="grupo" disabled readonly="true" value="Seleccione un grupo"> </p>


<!-- <div id="txtHint"></div> -->
<br>
<div id="alumnos"></div>


</body>
</html>