<html>
  <head>
      
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
       google.charts.load('current', {'packages':['bar'],'language': 'es'});
       google.charts.setOnLoadCallback(drawChart);
      
        

    function drawChart() {
       var data = google.visualization.arrayToDataTable([ 
        //  ['Mes', 'Sales', 'Expenses', 'Profit'],
        //  ['2014', 1000, 400, 200],
        //  ['2015', 1170, 460, 250],
        //  ['2016', 660, 1120, 300],
        //   ['2017', 1030, 540, 350],
        //  ['Totales', 4000, 3540, 350]
        // ]);
      

       <?php
        header("Content-Type: text/html;charset=utf-8");
        setlocale(LC_TIME, 'spanish');
        

        $conexion=mysqli_connect("localhost","root","","asistencias") or die ("Problema con la conexion");

        mysqli_select_db($conexion,"asistencias")or die("Cannot Select  Bass"); 

        $q = $_POST['q'];
        $mes = $_POST['mes'];
        $mesnum= date('m',strtotime($mes));
        $mes=strftime("%B",strtotime($mes));
        $sql = "select DISTINCT ID_Subgrupo from  CargaMaestro where  CargaMaestro.ID_Maestro= 1022";
        
        //echo "data.addColumn('number', '".$mes."');";
        if(date('m')>=7){
            $fechain=8;
            $fechafin=(date('m'));
         }
         else{
            $fechain=1;
            $fechafin=(date('m'));
         }
        
                 
         $matarray= array();
         $numarray= array();
         $totales= array();
         $x=0;
         
                // echo $value;
                
                 $ii=0;
                 

                 
                     
                    $sql2="SELECT paterno,materno,  COUNT(`Asistencia`)as faltas FROM `lista`, alumno WHERE lista.numeroControl= alumno.numeroControl and `ID_Subgrupo`= '".$q."' and Asistencia=0  GROUP by paterno, materno ORDER by paterno";//COUNT(`Asistencia`) desc";
                    // $sql2 = "SELECT count(`Asistencia`) FROM `lista` WHERE `ID_Subgrupo`= '".$value."' and MONTH(`fecha`) = ".$i."  and `Asistencia` = 0";
                     $result2 = mysqli_query($conexion,$sql2) or die("lul");
                     if (mysqli_num_rows($result2) > 0) {
                    
                     while($row = mysqli_fetch_assoc($result2)) {
                         $nombre= $row["paterno"]." ".$row["materno"];
                         $nombre= utf8_encode($nombre);
                         $numarray[$ii][0]=$nombre;
                         $numarray[$ii][1]=$row["faltas"];
                         $ii++;
                       }
                       
                   }
                     
                 echo  "['Faltas totales', '".$q."'], ";    
          
          $matarrcount=count($matarray);
          $tablerow=count($numarray);
          $tablecol=count($numarray[0]);
          $totcount=count($totales);
         

          for ($i = 0; $i < $tablerow; $i++) {
            echo '[';
            for ($j = 0; $j <$tablecol; $j++) {
                if ($j==0){
                        echo "'";
                        echo $numarray[$i][$j];
                        echo "'";
                }
                else
                    echo $numarray[$i][$j];
                
              if ($j!=$tablecol-1)
              {
                echo ',';
              }
            }
            echo ']';
            if ($i!=$tablerow-1)
            {
              echo ',';
            }
            
          }

         

          echo ']);';
          

      

                 

       ?>
    

       
 
       var options = {
         chart: {
           title: 'Faltas totales por Alumno',
           subtitle: 'Materia <?php echo $q;?>'
         },
         titleTextStyle: {
        color: 'black'
    },
    subtitleTextStyle: {
        color: 'black'
    },
    hAxis: {
        textStyle: {
            color: 'black'
        },
        titleTextStyle: {
            color: 'black',fontSize: 16
        }
    },
    vAxis: {
        textStyle: {
            color: 'black'
        },
        titleTextStyle: {
            color: 'black'
        }
    },
    legend: {
        textStyle: {
            color: 'black'
        }
    },
    
         width: 900,
         height: 500
       };
 
       var chart = new google.charts.Bar(document.getElementById('alumnos'));
 
       chart.draw(data, google.charts.Bar.convertOptions(options));
     }
    </script>
  </head>
  <body>
    <div id="chart" style="width: 800px; height: 500px;"></div>
    
    
  </body>
</html>

