<?php 
session_start();
if(!isset($_SESSION["session_username"])) {
	header("location:login.php");
} else {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div id="welcome">	
	<h2>Bienvenido, <span><?php echo $_SESSION['session_username'];?>! </span></h2>
	<p><a href="logout.php">Finalice</a> sesión aquí!</p>
</div>
</body>
</html>

<?php
}
?>
